import sys

import yaml
import os
import argparse


class Check:

    def __init__(self, config_file):
        self.data = None
        self.file = config_file
        self.load_yaml()

    def load_yaml(self):

        with open(self.file, 'r') as stream:
            try:
                self.data = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
            print(self.data)

    def show_config(self):
        config = self.data['profile']
        for key, value in config.items():
            print(key, value)


def main(argv):
    file = ""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", metavar="filepath", help="Config file", required=False, type=str)
    args = parser.parse_args()
    file = args.config
    print(file)

    if file is not None:
        check = Check(file)
    else:
        if 'FICHIER_DE_CONFIGURATION' in os.environ:
            file = os.environ['FICHIER_DE_CONFIGURATION']
        else:

            print("No configuration file")
            file = 'config.yaml'
        check = Check(file)

    check.show_config()


if __name__ == '__main__':
    main(sys.argv)
