package trois_tiers;


import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {

            System.out.println("------------------------------");
            System.out.println("Charger configuration.xml");
            ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("/trois_tier/configuration.xml");
            System.out.println("Obtenir presentation" );
            Presentation presentation = ctx.getBean("presentation", Presentation.class);
            System.out.println(presentation);
            System.out.println("Fermer configuration.xml");
            ctx.close();
    }
}
