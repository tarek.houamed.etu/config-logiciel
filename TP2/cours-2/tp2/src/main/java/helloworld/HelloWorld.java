package helloworld;
public class HelloWorld
{
    private String message;
    
    public HelloWorld()
    {
        System.out.println("Appel du constructeur HelloWorld()");
    }
    
    public static HelloWorld creerInstance()
    {
        System.out.println("Appel de HelloWorld.creerInstance()");
        return new HelloWorld();
    }
    
    public HelloWorld(String message)
    {
        System.out.println("Appel du constructeur HelloWorld(message=" + message +")");
        this.message = message;
    }
    
    public void setMessage(String message)
    {
        System.out.println("Appel de HelloWorld.setMessage(message=" + message +")");
        this.message = message;
    }
    
    public String getMessage()
    {
        return this.message;
    }
    
    public void initialisation()
    {
        System.out.println("Appel de HelloWorld.initialisation()");
    }
    
    public void destruction()
    {
        System.out.println("Appel de HelloWorld.destruction()");
    }
}
