package helloworld;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main
{
    public static void main( String[] args ) throws Exception
    {
        for(int i=1; i<11; i++) {
            System.out.println("------------------------------");
            System.out.println("Charger configuration" + i + ".xml");
            ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("helloworld/configuration" + i + ".xml");
            System.out.println("Obtenir helloworld" + i);
            HelloWorld helloworld = ctx.getBean("helloworld" + i, HelloWorld.class);
            System.out.println("Fermer configuration" + i + ".xml");
            ctx.close();
        }
    }
}
